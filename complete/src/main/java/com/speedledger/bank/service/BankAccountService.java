package com.speedledger.bank.service;

import com.speedledger.bank.model.BankAccount;
import com.speedledger.bank.repository.BankAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BankAccountService {

    @Autowired
    private BankAccountRepository bankAccountRepository;

    public List<BankAccount> getBankAccountList(){
        return bankAccountRepository.retrieveBankAccountList();
    }

    public Long getDefaultAccountId(){

        BankAccount potentialDefaultAccount = getDefaultBankAccount();

        if (potentialDefaultAccount == null ||
                potentialDefaultAccount.isSynthetic() ||
                potentialDefaultAccount.getBalance() < 0){
            return  null;
        }

        return potentialDefaultAccount.getId();
    }

    private BankAccount getDefaultBankAccount() {

        List<BankAccount> bankAccounts = bankAccountRepository.retrieveBankAccountList();

        if (bankAccounts.size() == 1){
            return bankAccounts.get(0);
        }
        else if (bankAccounts.size() > 1){
            return determineHighestBalance(bankAccounts, 0, bankAccounts.get(0));
        }
        else {
            return null;
        }
    }

    // This function defined recursive to avoid using "for loops". Instead of using "nested for",
    // in forwarding direction into recursive functions, the goal is to determine the account with
    // the highest balance (if there is any), and in the returning upward out of each function, the
    // goal is to check if the selected account, has a balance amount at least as twice as every
    // other account's balance.
    private BankAccount determineHighestBalance(List<BankAccount> bankAccounts, int i, BankAccount highest) {

        if (i == bankAccounts.size()){
            return highest;
        }

        if (Double.compare(highest.getBalance() , bankAccounts.get(i).getBalance()) < 0)
            highest = bankAccounts.get(i);

        highest = determineHighestBalance(bankAccounts, i+1, highest);

        if (highest == null) return null;

        if (Double.compare(highest.getBalance() , bankAccounts.get(i).getBalance() * 2) > 0 ||
                highest.equals(bankAccounts.get(i)))
            return highest;
        else
            return null;
    }
}

