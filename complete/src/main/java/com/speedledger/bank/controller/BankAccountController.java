package com.speedledger.bank.controller;

import java.util.List;

import com.speedledger.bank.model.BankAccount;
import com.speedledger.bank.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BankAccountController {

    @Autowired
    private BankAccountService bankAccountService;

    @RequestMapping(value = "/bankaccounts", method = RequestMethod.GET)
    public ResponseEntity<List<BankAccount>> getBankAccounts() {
        return new ResponseEntity<>(bankAccountService.getBankAccountList(), HttpStatus.OK);
    }

    @RequestMapping(value = "/bankaccounts/default", method = RequestMethod.GET)
    public ResponseEntity<Long> getDefaultAccountId(){
        return new ResponseEntity<>(bankAccountService.getDefaultAccountId(), HttpStatus.OK);
    }
}

