package com.speedledger.bank.model;

public class BankAccount {
    private final Long id;
    private final String number;
    private final String name;
    private final double balance;
    private final boolean creditCard;
    private final boolean synthetic;

    public BankAccount(Long id, String number, String name, double balance, boolean creditCard, boolean synthetic) {
        this.id = id;
        this.number = number;
        this.name = name;
        this.balance = balance;
        this.creditCard = creditCard;
        this.synthetic = synthetic;
    }

    public Long getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public double getBalance() {
        return balance;
    }

    public boolean isCreditCard() {
        return creditCard;
    }

    public boolean isSynthetic() {
        return synthetic;
    }
}
