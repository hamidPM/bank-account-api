package com.speedledger.bank.repository;

import com.speedledger.bank.model.BankAccount;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class BankAccountRepository {

    private static List<BankAccount> bankAccountList = new ArrayList<>();

    static  {
        BankAccount b1 = new BankAccount(new Long(1),
                "5536845312",
                String.format("Hamid P.Moghadam"),
                3490.34,
                true,
                false
        );
        BankAccount b2 = new BankAccount(new Long(2),
                "1358106084",
                String.format("Julia Roberts"),
                -8000,
                true,
                true
        );
        BankAccount b3 = new BankAccount(new Long(3),
                "22222222",
                String.format("Jack Johnson"),
                -34900.34,
                false,
                false
        );
        bankAccountList.add(b1);
        bankAccountList.add(b2);
        bankAccountList.add(b3);
    }

    public List<BankAccount> retrieveBankAccountList() {
        return bankAccountList;
    }
}

