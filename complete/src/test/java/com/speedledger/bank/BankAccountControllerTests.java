package com.speedledger.bank;

import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.speedledger.bank.model.BankAccount;
import com.speedledger.bank.repository.BankAccountRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BankAccountControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BankAccountRepository bankAccountRepository;

    @Test
    public void getBankAccountsTest() throws Exception {

        List<BankAccount> bankAccountList = new ArrayList<>();
        BankAccount b1 = new BankAccount(new Long(1),
                "423415315",
                String.format("Jack Johnson"),
                3490.34,
                true,
                false
        );
        BankAccount b2 = new BankAccount(new Long(2),
                "2222222233",
                String.format("Julia Roberts"),
                -8000,
                true,
                false
        );
        BankAccount b3 = new BankAccount(new Long(3),
                "3710823736",
                String.format("Hamid P.Moghadam"),
                34900.34,
                true,
                false
        );
        bankAccountList.add(b1);
        bankAccountList.add(b2);
        bankAccountList.add(b3);

        given(bankAccountRepository.retrieveBankAccountList()).willReturn(bankAccountList);

        this.mockMvc.perform(get("/bankaccounts")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].number", is("423415315")))
                .andExpect(jsonPath("$[0].name", is("Jack Johnson")))
                .andExpect(jsonPath("$[0].balance", is(new Double(3490.34))))
                .andExpect(jsonPath("$[0].creditCard", is(true)))
                .andExpect(jsonPath("$[0].synthetic",is(false)))
                .andExpect(jsonPath("$[1].id", is(2)))
                .andExpect(jsonPath("$[1].number", is("2222222233")))
                .andExpect(jsonPath("$[1].name", is("Julia Roberts")))
                .andExpect(jsonPath("$[1].balance", is(new Double(-8000))))
                .andExpect(jsonPath("$[1].creditCard", is(true)))
                .andExpect(jsonPath("$[1].synthetic",is(false)))
                .andExpect(jsonPath("$[2].id", is(3)))
                .andExpect(jsonPath("$[2].number", is("3710823736")))
                .andExpect(jsonPath("$[2].name", is("Hamid P.Moghadam")))
                .andExpect(jsonPath("$[2].balance", is(new Double(34900.34))))
                .andExpect(jsonPath("$[2].creditCard", is(true)))
                .andExpect(jsonPath("$[2].synthetic",is(false)))
        ;
    }

    @Test
    public void getDefaultTestWithDefaultAccountAsThirdObject() throws Exception {

        List<BankAccount> bankAccountList = new ArrayList<>();
        BankAccount b1 = new BankAccount(new Long(1),
                "423415315",
                String.format("Jack Johnson"),
                3490.34,
                true,
                false
        );
        BankAccount b2 = new BankAccount(new Long(2),
                "9234723610",
                String.format("Julia Roberts"),
                -8000,
                true,
                false
        );
        BankAccount b3 = new BankAccount(new Long(3),
                "2222222233",
                String.format("Hamid P.Moghadam"),
                34900.34,
                true,
                false
        );
        bankAccountList.add(b1);
        bankAccountList.add(b2);
        bankAccountList.add(b3);

        given(bankAccountRepository.retrieveBankAccountList()).willReturn(bankAccountList);

        this.mockMvc.perform(get("/bankaccounts/default")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$").value(3));
    }

    @Test
    public void getDefaultTestWithDefaultAccountAsFirstObject() throws Exception {

        List<BankAccount> bankAccountList = new ArrayList<>();
        BankAccount b1 = new BankAccount(new Long(1),
                "423415315",
                String.format("Jack Johnson"),
                3490.34,
                true,
                false
        );
        BankAccount b2 = new BankAccount(new Long(2),
                "9234723610",
                String.format("Julia Roberts"),
                -8000,
                true,
                false
        );
        BankAccount b3 = new BankAccount(new Long(3),
                "2222222233",
                String.format("Hamid P.Moghadam"),
                34900.34,
                true,
                false
        );
        bankAccountList.add(b3);
        bankAccountList.add(b1);
        bankAccountList.add(b2);

        given(bankAccountRepository.retrieveBankAccountList()).willReturn(bankAccountList);

        this.mockMvc.perform(get("/bankaccounts/default")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$").value(3));
    }

    @Test
    public void getDefaultTestWithDefaultAccountAsSecondObject() throws Exception {

        List<BankAccount> bankAccountList = new ArrayList<>();
        BankAccount b1 = new BankAccount(new Long(1),
                "423415315",
                String.format("Jack Johnson"),
                3490.34,
                true,
                false
        );
        BankAccount b2 = new BankAccount(new Long(2),
                "9234723610",
                String.format("Julia Roberts"),
                -8000,
                true,
                false
        );
        BankAccount b3 = new BankAccount(new Long(3),
                "2222222233",
                String.format("Hamid P.Moghadam"),
                34900.34,
                true,
                false
        );
        bankAccountList.add(b1);
        bankAccountList.add(b3);
        bankAccountList.add(b2);

        given(bankAccountRepository.retrieveBankAccountList()).willReturn(bankAccountList);

        this.mockMvc.perform(get("/bankaccounts/default")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$").value(3));
    }

    @Test
    public void getDefaultTestWithNoAccount() throws Exception {

        List<BankAccount> bankAccountList = new ArrayList<>();

        given(bankAccountRepository.retrieveBankAccountList()).willReturn(bankAccountList);

        this.mockMvc.perform(get("/bankaccounts/default")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    public void getDefaultTestWithOnlyOneAccountNormal() throws Exception {

        List<BankAccount> bankAccountList = new ArrayList<>();
        BankAccount b1 = new BankAccount(new Long(2),
                "423415315",
                String.format("Jack Johnson"),
                3490.34,
                true,
                false
        );
        bankAccountList.add(b1);

        given(bankAccountRepository.retrieveBankAccountList()).willReturn(bankAccountList);

        this.mockMvc.perform(get("/bankaccounts/default")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$").value(2));
    }

    @Test
    public void getDefaultTestWithOnlyOneAccountNegativeBalance() throws Exception {

        List<BankAccount> bankAccountList = new ArrayList<>();
        BankAccount b1 = new BankAccount(new Long(2),
                "423415315",
                String.format("Jack Johnson"),
                -3490.34,
                true,
                false
        );

        bankAccountList.add(b1);

        given(bankAccountRepository.retrieveBankAccountList()).willReturn(bankAccountList);

        this.mockMvc.perform(get("/bankaccounts/default")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    public void getDefaultTestWithOnlyOneAccountSynthetic() throws Exception {

        List<BankAccount> bankAccountList = new ArrayList<>();
        BankAccount b1 = new BankAccount(new Long(2),
                "423415315",
                String.format("Jack Johnson"),
                3490.34,
                true,
                true
        );

        bankAccountList.add(b1);

        given(bankAccountRepository.retrieveBankAccountList()).willReturn(bankAccountList);

        this.mockMvc.perform(get("/bankaccounts/default")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    public void getDefaultTestWithNegativeBalance() throws Exception {

        List<BankAccount> bankAccountList = new ArrayList<>();
        BankAccount b1 = new BankAccount(new Long(1),
                "423415315",
                String.format("Jack Johnson"),
                -3490.34,
                true,
                false
        );
        BankAccount b2 = new BankAccount(new Long(2),
                "9234723610",
                String.format("Julia Roberts"),
                -8000,
                true,
                false
        );
        BankAccount b3 = new BankAccount(new Long(3),
                "2222222233",
                String.format("Hamid P.Moghadam"),
                -34900.34,
                true,
                false
        );
        bankAccountList.add(b1);
        bankAccountList.add(b2);
        bankAccountList.add(b3);

        given(bankAccountRepository.retrieveBankAccountList()).willReturn(bankAccountList);

        this.mockMvc.perform(get("/bankaccounts/default")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    public void getDefaultTestWithNoDefaultAccount() throws Exception {

        List<BankAccount> bankAccountList = new ArrayList<>();
        BankAccount b1 = new BankAccount(new Long(1),
                "423415315",
                String.format("Jack Johnson"),
                4490.34,
                true,
                false
        );
        BankAccount b2 = new BankAccount(new Long(2),
                "9234723610",
                String.format("Julia Roberts"),
                8000,
                true,
                false
        );
        BankAccount b3 = new BankAccount(new Long(3),
                "2222222233",
                String.format("Hamid P.Moghadam"),
                -34900.34,
                true,
                false
        );
        bankAccountList.add(b1);
        bankAccountList.add(b2);
        bankAccountList.add(b3);

        given(bankAccountRepository.retrieveBankAccountList()).willReturn(bankAccountList);

        this.mockMvc.perform(get("/bankaccounts/default")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    public void getDefaultTestWithZeroBalanceAccount() throws Exception {

        List<BankAccount> bankAccountList = new ArrayList<>();
        BankAccount b1 = new BankAccount(new Long(1),
                "423415315",
                String.format("Jack Johnson"),
                -3490.34,
                true,
                false
        );
        BankAccount b2 = new BankAccount(new Long(2),
                "9234723610",
                String.format("Julia Roberts"),
                1,
                true,
                false
        );
        BankAccount b3 = new BankAccount(new Long(3),
                "2222222233",
                String.format("Hamid P.Moghadam"),
                0,
                true,
                false
        );
        bankAccountList.add(b1);
        bankAccountList.add(b2);
        bankAccountList.add(b3);

        given(bankAccountRepository.retrieveBankAccountList()).willReturn(bankAccountList);

        this.mockMvc.perform(get("/bankaccounts/default")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$").value(2));
    }

    @Test
    public void getDefaultTestWithAllAccountsHaveZeroBalance() throws Exception {

        List<BankAccount> bankAccountList = new ArrayList<>();
        BankAccount b1 = new BankAccount(new Long(1),
                "423415315",
                String.format("Jack Johnson"),
                0,
                true,
                false
        );
        BankAccount b2 = new BankAccount(new Long(2),
                "9234723610",
                String.format("Julia Roberts"),
                0,
                true,
                false
        );
        BankAccount b3 = new BankAccount(new Long(3),
                "2222222233",
                String.format("Hamid P.Moghadam"),
                0,
                true,
                false
        );
        bankAccountList.add(b1);
        bankAccountList.add(b2);
        bankAccountList.add(b3);

        given(bankAccountRepository.retrieveBankAccountList()).willReturn(bankAccountList);

        this.mockMvc.perform(get("/bankaccounts/default")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    public void getDefaultTestWithNoPositiveBalanceAccount() throws Exception {

        List<BankAccount> bankAccountList = new ArrayList<>();
        BankAccount b1 = new BankAccount(new Long(1),
                "423415315",
                String.format("Jack Johnson"),
                -3490.34,
                true,
                false
        );
        BankAccount b2 = new BankAccount(new Long(2),
                "9234723610",
                String.format("Julia Roberts"),
                -8000,
                true,
                false
        );
        BankAccount b3 = new BankAccount(new Long(3),
                "2222222233",
                String.format("Hamid P.Moghadam"),
                0,
                true,
                false
        );
        bankAccountList.add(b1);
        bankAccountList.add(b2);
        bankAccountList.add(b3);

        given(bankAccountRepository.retrieveBankAccountList()).willReturn(bankAccountList);

        this.mockMvc.perform(get("/bankaccounts/default")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$").value(3));
    }

    @Test
    public void getDefaultTestWithTwoEqualPositiveBalanceAccounts() throws Exception {

        List<BankAccount> bankAccountList = new ArrayList<>();
        BankAccount b1 = new BankAccount(new Long(1),
                "423415315",
                String.format("Jack Johnson"),
                3490.34,
                true,
                false
        );
        BankAccount b2 = new BankAccount(new Long(2),
                "9234723610",
                String.format("Julia Roberts"),
                -8000,
                true,
                false
        );
        BankAccount b3 = new BankAccount(new Long(3),
                "2222222233",
                String.format("Hamid P.Moghadam"),
                3490.34,
                true,
                false
        );
        bankAccountList.add(b1);
        bankAccountList.add(b2);
        bankAccountList.add(b3);

        given(bankAccountRepository.retrieveBankAccountList()).willReturn(bankAccountList);

        this.mockMvc.perform(get("/bankaccounts/default")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    public void getDefaultTestWithDefaultAccountBeingSynthetic() throws Exception {

        List<BankAccount> bankAccountList = new ArrayList<>();
        BankAccount b1 = new BankAccount(new Long(1),
                "423415315",
                String.format("Jack Johnson"),
                -3490.34,
                true,
                false
        );
        BankAccount b2 = new BankAccount(new Long(2),
                "9234723610",
                String.format("Julia Roberts"),
                230,
                true,
                false
        );
        BankAccount b3 = new BankAccount(new Long(3),
                "2222222233",
                String.format("Hamid P.Moghadam"),
                3490.34,
                true,
                true
        );
        bankAccountList.add(b1);
        bankAccountList.add(b2);
        bankAccountList.add(b3);

        given(bankAccountRepository.retrieveBankAccountList()).willReturn(bankAccountList);

        this.mockMvc.perform(get("/bankaccounts/default")).andDo(print()).andExpect(status().isOk())
                .andExpect(jsonPath("$").doesNotExist());
    }
}
